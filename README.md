# pascal

Object Pascal: Free Pascal, Delphi, Turbo Pascal, Mac Pascal

* [*Category:Free Pascal*
  ](https://www.rosettacode.org/wiki/Category:Free_Pascal) (Rosetta Code)

# Features
## Memory management
* [*Freeing classes*
  ](https://castle-engine.io/modern_pascal_introduction.html#_freeing_classes)
  * Chapter from: [*Modern Object Pascal Introduction for Programmers*
    ](https://castle-engine.io/modern_pascal_introduction.html)
    (2021-12) Michalis Kamburelis
* [*TComponent*
  ](https://www.freepascal.org/docs-html/rtl/classes/tcomponent.html)
* [Pascal - Memory Management](https://www.tutorialspoint.com/pascal/pascal_memory.htm)

# Functional programming in Free Pascal
* [*Feature announcement: Function References and Anonymous Functions*
  ](https://forum.lazarus.freepascal.org/index.php?topic=59468.0)
  (2022-05) forum.lazarus.freepascal.org
  * It will be in the next major release which is not yet scheduled. The next release will be a minor release (namely 3.2.4).
    * 3.3 Trunk
    * 3.4 Stable probably
* [*Topic: Functional programming in Pascal*
  ](https://forum.lazarus.freepascal.org/index.php?topic=45818.0)
  (2019) forum.lazarus.freepascal.org

